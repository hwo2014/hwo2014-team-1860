import json
import math
import time

from racer import Racer

class PRaceBot(Racer):

    def __init__(self, socket, name, key, track_name = '', number_of_cars = 0):
        self.socket = socket
        self.name = name
        self.key = key
        self.throt = 1
        self.current_piece_index = 0
        self.next_throttle=0
        self.last_throttle=0
        self.tick_of_game = 0
        self.last_in_piece_distance = 0
        self.last_piece_index=0
        self.total_distance=0
        self.last_distance = 0
        self.last_speed = 0
        self.last_acceleration = 0
        #should I switch lanes?
        self.switch = False
        self.target_lane = 0
        self.last_angle = 0
        self.stats = []
        self.turbo = 0
        self.turbo_on = False
        self.current_in_piece_distance = 0
        self.current_lane = 0
        self.track_name = track_name
        self.number_of_cars =  number_of_cars
        self.max_speed = 0
        #if(name[-1:] in ['1','2','3','4','5','6']):
        #    self.number_of_cars = name[-1:]
        self.car_to_hit = False
        self.car_to_pass = False
        self.max_lap_angle = 0
        self.f_corrected = False
        #keimola
        #self.f = 1550
        self.f_correction = 0
        self.f=1250

        self.k = list()
        #France,
        #self.f = 1380
        #Germany
        #self.f = 1500
        #USA
        #self.f = 1700


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):

        if(self.number_of_cars == 0):
            return self.join_simple()

        joinData = {
            "botId": {
                "name": self.name,
                "key": self.key
            },
            "carCount": self.number_of_cars,
            "trackName": self.track_name,

           # "password": "dupa111"
        }
        print(joinData)

        return self.msg("joinRace", joinData)


    def join_simple(self):
        joinParams = {"name": self.name, "key": self.key}
        return self.msg("join", joinParams)

    def throttle(self, throttle):
        self.next_throttle = throttle

        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        print data['race']['cars']
        track = data['race']['track']
        # adhension factor
        # from my calculations f is safe with 1840 but with 2054 car gets out of track sometimes
        #f = 1601
        #f = 1530
        min_radius = min([ x['radius'] for x in track['pieces'] if x.has_key('angle')])
        #print min_radius
        self.f += min_radius
        print 'starting with f: %s' % (self.f,)
        f = self.f
        #f = 1700
        for piece in track['pieces']:
            piece["vmax"] = [ 3000 for i in track["lanes"] ]
            if 'length' not in piece:
                 piece["vmax"] = [ math.sqrt( f * ( piece["radius"] - ( piece["angle"] / math.fabs(piece["angle"] ) ) * i["distanceFromCenter"] ) ) for i in track["lanes"] ]

                 piece["length"] = [ math.fabs(math.pi * piece["angle"] * ( piece["radius"] - ( piece["angle"] / math.fabs(piece["angle"] ) ) * i["distanceFromCenter"] ) ) / 180.0 for i in track["lanes"] ]
            else:
                 piece["length"] = [ piece["length"] for i in track["lanes"] ]
            #print piece

        self.track = track


        self.number_of_pieces = len(self.track['pieces'])
        self.track['pieces'] = self.track['pieces']+self.track['pieces']

        # print(track)

        # self.cars = data['race']['cars']
        # self.session = data['race']['raceSession']
        self.ping()

    def distance_traveled_since_last_frame(self, pieceIndex, inPieceDistance, laneIndex = 0):
        # print("pieceIndex %d, inPieceDistance %f this piece length: %f" %  (pieceIndex, inPieceDistance, self.get_piece_length(pieceIndex)
        if pieceIndex!=self.last_piece_index:
             return (self.get_piece_length(self.last_piece_index, laneIndex) - self.last_in_piece_distance) + inPieceDistance
        else:
            return inPieceDistance - self.last_in_piece_distance

    def get_piece_length(self, pieceIndex, laneIndex = 0):
        #FIXME: support for laneIndex
        return self.track["pieces"][pieceIndex]["length"][laneIndex]


    def indexes_of_next_curves(self):
        return [(index % self.number_of_pieces) for index,piece in enumerate(self.track["pieces"]) if index>self.current_piece_index and 'radius' in piece]
        
    def distance_to_begining_of_next_curve(self):
        #next_curve_index = self.indexes_of_next_curves()[0]

        next_curve_index = [ x for x,y in enumerate(self.track["pieces"][self.current_piece_index+1:]) if y.has_key('radius')][0]

        lls = [piece['length'][self.current_lane] for piece in self.track["pieces"][self.current_piece_index:self.current_piece_index+next_curve_index+1]]
        #lls = [ piece['length'][self.current_lane] for piece in self.track["pieces"][self.current_piece_index:self.current_piece_index+next_curve_index+1] ]
        #print lls
        return (sum(lls)-self.current_in_piece_distance,self.track['pieces'][self.current_piece_index+next_curve_index+1])


    def distance_to_middle_of_next_curve(self):
        next_curve_index = self.indexes_of_next_curves()[0]
        lls = [piece['length'] for piece in self.track["pieces"][self.current_piece_index:next_curve_index]]
        sums = [sum(x)-self.current_in_piece_distance for x in zip(*lls)]
        curve_lengths = self.track["pieces"][next_curve_index]['length']
        final_sums = sums = [sum(x) for x in zip(*[sums,[ x/2.0 for x in curve_lengths]])]
        return final_sums

    def on_car_positions_parent(self, data):

        #car_in_front = [ x for x in data if x['id']['name'] != self.name and x['piece'] ]


        #t0 = time.time()
        data_my = [ x for x in data if x['id']['name'] == self.name ][0]
        self.current_lane = data_my['piecePosition']['lane']['endLaneIndex']
        self.last_throttle = self.next_throttle
        frame_time = (1.0/60.0)

        # print(data)
        self.current_piece_index = data_my["piecePosition"]["pieceIndex"]
        self.current_in_piece_distance = data_my["piecePosition"]["inPieceDistance"]

        current_distance = self.distance_traveled_since_last_frame(self.current_piece_index, self.current_in_piece_distance, data_my["piecePosition"]["lane"]["endLaneIndex"])
        current_piece_length = self.get_piece_length(self.current_piece_index, data_my["piecePosition"]["lane"]["endLaneIndex"])


        self.total_distance += current_distance
        speed = current_distance / frame_time
        acceleration = (speed - self.last_speed) / frame_time

        # print("Kinetic stats:")
        # print(self.current_piece_index, current_distance, self.current_in_piece_distance, speed, acceleration)
        # print(speed)
        # print(acceleration)
        # print(str(self.tick_of_game) + "," + str(self.current_piece_index) + "," + str(speed) + "," + str(self.track['pieces'][self.current_piece_index]['vmax'][self.current_lane]) + "," + str(acceleration))

        # attrs = vars(self)
        # print ', '.join("%s: %s" % item for item in attrs.items())




        self.tick_of_game+=1
        self.last_piece_index = self.current_piece_index
        self.last_in_piece_distance = self.current_in_piece_distance
        self.last_distance = current_distance
        self.last_speed = speed

        #if self.max_speed<speed and not self.turbo_on:
         #   self.max_speed=speed
           # print self.max_speed
        self.last_acceleration = acceleration
        angle = abs(data_my['angle'])

        if angle>55 and not self.f_corrected and angle>self.last_angle:
            self.correct_f(-10)
            self.f_correction = 50
        else:
            self.f_corrected = False

        self.max_lap_angle = max(self.max_lap_angle, angle)

        #if len(self.k)<3 and self.last_speed >0:
        #    self.k.append(self.last_speed)
        #else:
        #o    print self.k

        car_in_front = [ x for x in data if x['id']['name'] != self.name and self.hit_him(x) ]
        if len(car_in_front)>0:
            self.car_to_hit=True
        else:
            self.car_to_hit=False

        car_in_front = [ x for x in data if x['id']['name'] != self.name and self.pass_him(x) ]
        if len(car_in_front)>0:
            self.car_to_pass=True
        else:
            self.car_to_pass=False
        self.on_car_positions([data_my])
        #t1 = time.time()
        #print t1-t0

    def distance_to_car(self, x):
        if x["piecePosition"]["pieceIndex"] == self.current_piece_index:
            distance = x["piecePosition"]["inPieceDistance"] - self.current_in_piece_distance
        elif x["piecePosition"]["pieceIndex"] == self.current_piece_index+1:
            distance = x["piecePosition"]["inPieceDistance"] + self.track['pieces'][self.current_piece_index]['length'][self.current_lane] - self.current_in_piece_distance
        else:
            distance = 10000
        return distance



    def hit_him(self, x):
        dist = self.distance_to_car(x)
        if dist < 0:
            return False

        if x["piecePosition"]["lane"]["endLaneIndex"]==self.current_lane and  dist > 10 and dist<50 and self.turbo and not self.track['pieces'][self.current_piece_index].has_key('angle')\
                or x["piecePosition"]["lane"]["endLaneIndex"]==self.current_lane and dist<5 and self.turbo and self.track['pieces'][self.current_piece_index].has_key('angle'):
            return True
        else:
            return False

    def pass_him(self, x):
        dist = self.distance_to_car(x)
        if x["piecePosition"]["lane"]["endLaneIndex"]==self.current_lane and  dist > 0 and dist<50:
            return True
        else:
            return False


    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_end_(self, data):
        print("Race ended")
        # print ("bbb")
        self.ping()

    def on_lap_finished(self,data):
        if data['car']['name']!= self.name:
            self.ping()
            return True
        print "Max angle on this lap: %s" % self.max_lap_angle

        if self.f_correction == 0:
            if self.max_lap_angle<10:
                self.correct_f(150)
            elif self.max_lap_angle<25:
                self.correct_f(100)
            elif self.max_lap_angle<40:
                self.correct_f(50)
            elif self.max_lap_angle<45:
                self.correct_f(25)
            elif self.max_lap_angle<50:
                self.correct_f(5)

         #   self.correct_f(10)

        self.max_lap_angle = 0
        print "LapFinished"
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
    def on_spawn(self, data):
        self.throttle(1)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions_parent,
            'crash': self.on_crash,
            'turboAvailable': self.on_turbo,
            'turboEnd': self.on_turbo_end,
            'gameEnd': self.on_game_end,
            'raceSession' : self.on_race_session,
            'lapFinished': self.on_lap_finished,
            'error': self.on_error,
            'spawn': self.on_spawn,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                print(msg['data'])
                self.ping()
            line = socket_file.readline()

    def do_we_switch(self):
        #return False


        if self.track['pieces'][self.current_piece_index].has_key('switch'):
            self.switch = False
            return False
        #switching already
        if self.turbo_on:
            return False

        if self.switch:
            return False

        if not self.track['pieces'][self.current_piece_index+1].has_key('switch'):
            return False

        # start first switch towards current piece
        # end second switch towards current piece
        start, end = [ self.current_piece_index + 1 + i for i, j in enumerate(self.track['pieces'][self.current_piece_index + 1:]) if j.get('switch',None)][:2]


        # calculating neighbour lanes - switch is available only to neighbour' lanes
        if self.current_lane == 0:
            l_lane = self.current_lane
        else:
            l_lane = self.current_lane - 1
        r_lane = min(self.current_lane + 1, len(self.track['lanes']) - 1)

        # print(str(self.current_piece_index), '. C: ', str(self.current_lane), ', L: ', str(l_lane), ', R:', str(r_lane))

        # changing lane costs extra time - Pitagorean theorem
        temp_track = self.track['pieces'][start:end]
        #print(temp_track)

        for i in range(l_lane, r_lane + 1):
            temp_track[0]['length'][i] = math.sqrt( math.pow(temp_track[0]['length'][i], 2) + math.pow(self.track['lanes'][i]['distanceFromCenter'] - self.track['lanes'][self.current_lane]['distanceFromCenter'], 2) )

        # counting time needed to overcome piece per lane - only neighbour lanes
        time_per_piece = [ [ i['length'][j]/i['vmax'][j] for j in range( len(i['length']) )] for i in temp_track ]

        # time for particular lane
        time_per_lane = [sum(list(i)) for i in zip(*time_per_piece)]

        # decision for fastest lane
        fastest_lane = time_per_lane.index( min(time_per_lane[l_lane:r_lane + 1]) )
        if self.car_to_pass and not self.car_to_hit:
            print "will try passing..."
            if self.current_lane == 0:
                return 'Right'
            else:
                return 'Left'

        if self.current_lane != fastest_lane :
            if self.current_lane < fastest_lane :
                return 'Right'
            else:
                return 'Left'

        return False

    def on_race_session(self, data):
        self.switch = False
