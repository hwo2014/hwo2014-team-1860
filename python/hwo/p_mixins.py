from racer import Racer
import math
try:
    import matplotlib
    matplotlib.use('TkAgg')
    import matplotlib.pyplot as plt
except:
    pass

class RacerMixIn(Racer):
    def on_car_positions(self, data):
        self.throttle(0.6)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()


class SkodaDriverMixIn(Racer):

    def on_car_positions(self, data):
        self.throttle(0.2)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

class RecklessDriverMixIn(Racer):

    def on_car_positions(self, data):
        self.throttle(1)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()


class PlotMixIn(Racer):

    # import matplotlib
    # matplotlib.use('TkAgg')
    # import matplotlib.pyplot as plt


    def on_game_end(self, data):

        print("Race ended")
        #print self.stats
        self.ping()

        X = [ x[0] for x in self.stats]
        T = [ x[1] for x in self.stats]
        S = [ x[2] for x in self.stats]
        S_m = [ x[3] for x in self.stats]
        Ac = [ x[4] for x in self.stats]
        An = [ x[5] for x in self.stats]

        print "Min acc: %s" % (min(Ac),)
        print "Max acc: %s" % (max(Ac),)
        print "Max ang: %s" % (max(An),)
        print "Final f: %s" % (self.f,)
        print "Max speed (  no turbo): %s" % (self.max_speed,)
        try:
            plt.xticks(range(0,3000,100))
            plt.yticks(range(0,1500, 50))
            plt.plot(X, T, color="blue", linewidth=1, linestyle="-", label="Throttle")
            plt.plot(X, S, color="green", linewidth=1, linestyle="-", label="Speed")
            plt.plot(X, S_m, color="red", linewidth=1, linestyle="-", label="Speed Max")
            plt.plot(X, Ac, color="black", linewidth=1, linestyle="-", label="Acc")
            plt.plot(X, An, color="orange", linewidth=1, linestyle="-", label="Ang")
            plt.legend(loc='upper left')
            plt.show()

        except:
            pass



class FirstDecentMixIn(Racer):
    def on_game_end(self, data):

        print("Race ended")
        #print self.stats
        self.ping()

    def correct_f(self, x):
        self.f +=x
        print "F changed by: %s to value: %s" % (x, self.f,)
        #recalulcate maxes:
        #f = self.f
        for piece in self.track['pieces']:
            if 'angle' in piece:
                piece["vmax"] = [ math.sqrt( self.f * ( piece["radius"] - ( piece["angle"] / math.fabs(piece["angle"] ) ) * 1 * i["distanceFromCenter"] ) ) for i in self.track["lanes"] ]
        return True

    def on_crash(self, data):
        print("Someone crashed")
        if self.name == data['name'] and not self.turbo_on:
            self.correct_f(-50)
            self.turbo_on = False
            print "WE CRASHED!"
        self.ping()


    def on_turbo(self, data):
        print 'got turbo!'
        #print data
        self.f_corrected = True
        self.turbo = data['turboDurationTicks']
        self.ping()

    def on_turbo_end(self, data):
        print 'Turbo ended!'
        self.f_corrected = False
        self.turbo_on = False
        self.ping()

    def on_game_end(self, data):
        print("game end")


    def on_car_positions(self, data, laneIndex = 0 ):
#        print data
        if self.last_speed < 10:
            self.throttle(1)
            return True

        switch = self.do_we_switch()

       # if(self.tick_of_game == 3 or self.throt == 0 and self.tick_of_game<57):
       #    switch = 'Right'


        if switch:
            #print switch
            self.msg('switchLane', switch)
            self.switch = True
            return True


        current_piece = self.track['pieces'][data[-1]['piecePosition']['pieceIndex']]
        current_lane = data[-1]['piecePosition']['lane']['endLaneIndex']
        next_piece = self.track['pieces'][data[-1]['piecePosition']['pieceIndex']+1]
        #print current_lane

        next_curve_distance = self.distance_to_begining_of_next_curve()
        #print next_curve_distance,next_curve_distance[1]['radius']
        #checking turbo
        #if self.turbo>0 and next_curve_distance[0]>300 and (not current_piece.has_key('angle') or current_piece['length'][self.current_lane]*0.5<self.last_in_piece_distance):
        #if self.turbo>0 and next_curve_distance[0]>350 and next_curve_distance[1]['radius']>50 and (current_piece.has_key('angle') and current_piece['length'][self.current_lane]*0.5<self.last_in_piece_distance):
        if self.turbo>0 and next_curve_distance[0]>350 and (current_piece.has_key('angle') and current_piece['length'][self.current_lane]*0.6<self.current_in_piece_distance):
        #if self.turbo>0 and next_curve_distance[0]>100 and self.car_in_front:
            self.turbo = 0
            self.turbo_on = True
            print "zapierdylam! : %s" % (next_curve_distance[0],)
            self.msg("turbo", "zapierdylam!")
            return True


        #if False and self.turbo>0 and next_curve_distance[0]>300 and (not current_piece.has_key('angle') or current_piece['length'][self.current_lane]*0.5<self.last_in_piece_distance):
        if self.turbo>0 and next_curve_distance[0]<100 and self.car_to_hit:
            self.turbo = 0
            self.turbo_on = True
            print "Bede stukac!"
            self.msg("turbo", "zapierdylam!")
            return True

        if self.last_speed > next_curve_distance[1]['vmax'][current_lane]:
            if self.turbo_on and next_curve_distance[1]['radius'] <=50:
                caution = 3.2
            elif self.turbo_on and next_curve_distance[1]['radius'] <=90:
                caution = 2.6
            elif self.turbo_on and next_curve_distance[1]['radius']<=150:
                caution = 1.4
            else:
                caution = 1
            breaking_distance = caution * (self.last_speed**2 - next_curve_distance[1]['vmax'][current_lane]**2)/self.f

        else:
            breaking_distance = 0
        #print breaking_distance, next_curve_distance[0]
        ang = abs(data[-1]['angle'])



        if not current_piece.has_key('angle'):

            if breaking_distance < next_curve_distance[0]:
                self.throt = 1
            else:
                self.throt = 0

        #elif ang<15 and self.last_angle >= ang:
         #   self.throt = 0

        elif current_piece.has_key('angle'):
            #if self.last_in_piece_distance < current_piece['length'][current_lane]*0.50:
            #or self.last_in_piece_distance < current_piece['length'][current_lane]*0.50)
            if self.last_speed < current_piece['vmax'][current_lane]  and breaking_distance < next_curve_distance[0]:
                self.throt = 1
            #elif self.last_in_piece_distance > current_piece['length'][current_lane]*0.50 and self.last_in_piece_distance < current_piece['length'][current_lane]*0.60:
            #    self.throt = 1
            else:
                self.throt = 0

            #else:
             #   if (next_piece.has_key('angle') and current_piece['angle']*next_piece['angle']>0) and self.last_speed < next_piece['vmax'][current_lane]:
              #      self.throt = 1
               # else:
                #    self.throt = 0



        self.last_angle = ang

        if abs(self.last_acceleration)>600:
            self.last_acceleration = 4

        self.stats.append((self.tick_of_game, self.throt*100, self.last_speed, current_piece['vmax'][current_lane], self.last_acceleration, self.last_angle*10))
#        if self.throt == 0:
#           self.throt = 0.3

        self.throttle(self.throt)



class PDriverMixIn(Racer):
    def on_turbo(self, data):
        print 'got turbo!'
        self.turbo = data['turboDurationTicks']
        self.ping()

    def on_turbo_end(self, data):
        print 'Turbo ended!'
        self.turbo_on = False
        self.throttle(0)

    def next_curve(self, data):
        pieceIndex = data[-1]['piecePosition']['pieceIndex']

        #for p in [ (x,y) for x,y in enumerate(self.track['pieces']) if y.has_key('switch') ]:
        #    print p
        if pieceIndex == 2 and data[-1]['piecePosition']['lane']['endLaneIndex'] == 0:
            self.switch = {u'Type': 'switchLane', 'data':'Right' }
        elif pieceIndex == 7 and data[-1]['piecePosition']['lane']['endLaneIndex'] == 1:
            self.switch = {u'Type': 'switchLane', 'data':'Left' }
        elif pieceIndex == 17 and data[-1]['piecePosition']['lane']['endLaneIndex'] == 0:
            self.switch = {u'Type': 'switchLane', 'data':'Right' }


    def next_curve2(self,data):
        #if self.last_speed>300:
        #    return False
        track = self.track['pieces']
        pieceIndex = data[-1]['piecePosition']['pieceIndex']
        next_curve = [ x for x in self.track['pieces'][pieceIndex:] if x.has_key('angle') ]

        if len(next_curve)==0:
            next_curve = [ x for x in self.track['pieces'] if x.has_key('angle') ]



        if len(next_curve)>0:
            next_curve = next_curve[0]
            if next_curve['angle']>0:
                desired_lane=1
            else:
                desired_lane=0

        #can I switch?
            #if desired_lane != data[-1]['piecePosition']['lane']['endLaneIndex'] and self.track['pieces'][data[-1]['piecePosition']['pieceIndex']].has_key('switch'):
            if desired_lane != self.target_lane and len([ x for x in track[pieceIndex : track.index(next_curve)+1]])>0:
                #print data[-1]['piecePosition']['lane']['endLaneIndex']
                self.target_lane = desired_lane
                if desired_lane == 1:
                    self.switch = {u'Type': 'switchLane', 'data':'Right' }
                if desired_lane == 0:
                    self.switch = {u'Type': 'switchLane', 'data':'Left' }
            else:
                self.switch = False

        else:
            self.switch = False


    def on_car_positions(self, data, laneIndex = 0 ):

        self.next_curve(data)
        #print self.track['pieces']
        #if self.switch and self.track['pieces'][data[-1]['piecePosition']['pieceIndex']].has_key('switch'):
        if self.switch:
            typ = self.switch['Type']
            mes = self.switch['data']
            #typ = 'switchLane'
            #mes = 'Right'
            self.switch = False
            self.msg(typ,mes)
            return True
        #if data[-1]['piecePosition']['pieceIndex']==34:
        #    self.throt = 1
        #    self.throttle(self.throt)
        #    return True

        ang = abs(data[-1]['angle'])
        current_piece = self.track['pieces'][data[-1]['piecePosition']['pieceIndex']]
        current_lane = data[-1]['piecePosition']['lane']['endLaneIndex']
        #print current_piece['vmax'][current_lane]

        if data[-1]['piecePosition']['pieceIndex'] == len(self.track['pieces'])-1:
            next_piece = self.track['pieces'][0]
        else:
            next_piece = self.track['pieces'][data[-1]['piecePosition']['pieceIndex']+1]

     #   if data[-1]['piecePosition']['pieceIndex'] == 34:
      #      print "aaaaaaaaaaaaa"
       #     print data[-1]['piecePosition']['inPieceDistance']
        #    print self.turbo

        if self.turbo>0 and data[-1]['piecePosition']['pieceIndex'] == 34 and data[-1]['piecePosition']['inPieceDistance']>45 :
            self.turbo = 0
            self.turbo_on = True
            print "zapierdylam!"
            self.msg("turbo", "zapierdylam!")
            return True


        if self.last_speed>580 and (data[-1]['piecePosition']['pieceIndex']==39 or data[-1]['piecePosition']['pieceIndex']==0 or data[-1]['piecePosition']['pieceIndex']==1 or data[-1]['piecePosition']['pieceIndex']==2 or data[-1]['piecePosition']['pieceIndex']==3):

            self.throt = 0
            self.throttle(self.throt)
            return True


        if 'angle' not in current_piece.keys() and 'angle' not in next_piece.keys():
            #if self.turbo_on:
            #    self.throt = 0.9
            #else:
            self.throt=1

        elif 'angle' not in next_piece.keys() and 'angle' in current_piece.keys() and data[-1]['piecePosition']['inPieceDistance'] > current_piece['length'][laneIndex] * 0.6:
            self.throt = 1

        elif 'angle' not in current_piece.keys() and 'angle' in next_piece.keys() and data[-1]['piecePosition']['inPieceDistance'] > current_piece['length'][laneIndex] * 0.1:
            if self.last_speed > next_piece['vmax'][current_lane]:
                self.throt = 0
            else:
                #if self.turbo_on:
                 #   self.throt = 0.9
                #else:
                self.throt=1


        else:
            if ang >-1: #and self.last_angle <= ang:
                #if self.last_speed > min(current_piece['vmax'][current_lane], next_piece['vmax'][current_lane]):
                if self.last_speed > current_piece['vmax'][current_lane]:
                    self.throt = 0
                else:
                    self.throt = 1
            elif ang < -1:
                self.throt = 1
        self.last_angle = ang
        if self.last_acceleration > 800:
            self.last_acceleration=800
        elif self.last_acceleration < -800:
            self.last_acceleration= -800
        #print self.throt, self.last_speed, current_piece['vmax'][current_lane]
        # print self.last_acceleration
        self.stats.append((self.tick_of_game, self.throt*100, self.last_speed, current_piece['vmax'][current_lane], self.last_acceleration))
        self.throttle(self.throt)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()


