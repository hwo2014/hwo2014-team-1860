import json
import socket
import sys
from hwo import p_bots, p_mixins


def MixIn(pyClass, mixInClass, makeLast=0):
  if mixInClass not in pyClass.__bases__:
    if makeLast:
      pyClass.__bases__ += (mixInClass,)
    else:
      pyClass.__bases__ = (mixInClass,) + pyClass.__bases__

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))

        MixIn(p_bots.PRaceBot, p_mixins.FirstDecentMixIn)
        #for ploting
        MixIn(p_bots.PRaceBot, p_mixins.PlotMixIn)

        #bot = p_bots.PRaceBot(s, name, key, track_name='imola', number_of_cars = 2)

        bot = p_bots.PRaceBot(s, name, key, track_name='', number_of_cars = 0)
        bot.run()
